" PLUGINS CONFIGURATION
" SYNTASTIC Settings {{{
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_check_on_wq = 0

" Load specific pylint configuration file
let g:syntastic_python_pylint_post_args = "--rcfile=../.pylintrc"

" Enable specific checkers for ruby
let g:syntastic_ruby_checkers = ["mri", "reek", "rubocop"]
" }}}
" VIMPLUG Settings {{{
  call plug#begin('~/.vim/bundle')
    Plug 'tpope/vim-rails'
    Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
    Plug 'https://github.com/moll/vim-bbye.git'
    Plug 'https://github.com/scrooloose/nerdtree.git'
    Plug 'https://github.com/scrooloose/nerdcommenter'
    Plug 'https://github.com/scrooloose/syntastic.git'
    Plug 'https://github.com/bling/vim-airline'
    Plug 'https://github.com/vim-airline/vim-airline-themes'
    Plug 'git://github.com/tpope/vim-bundler.git'
    Plug 'https://github.com/tpope/vim-fugitive'
    Plug 'https://github.com/plasticboy/vim-markdown.git'
    Plug 'https://git::@github.com/tpope/vim-rails.git'
    Plug 'git://github.com/tpope/vim-sensible.git'
    Plug 'git://github.com/slim-template/vim-slim.git'
    Plug 'https://git::@github.com/honza/vim-snippets.git'
    Plug 'git://github.com/tpope/vim-surround.git'
    Plug 'https://github.com/guns/xterm-color-table.vim.git'
    Plug 'airblade/vim-gitgutter'
  call plug#end()
	syntax on
	filetype plugin indent on
" }}}
" NERDTREE Settings {{{
	noremap <leader>n :NERDTreeToggle<cr>
  let g:NERDTreeWinSize=50
" }}}
" Airline Settings {{{
  let g:airline#extensions#tabline#enabled = 1
  let g:airline_section_error = ''
  let g:airline_theme='tomorrow'
" }}}

" Add the current directory to the path recursively. Usefull to make
" find/sfind work.
set path=$PWD/**

" Show all options with tab
set wildmode=longest,list,full

" default (utf-8)
if has("multi_byte")
  if &termencoding == ""
    let &termencoding = &encoding
  endif
  set encoding=utf-8
  set fileencoding=utf-8
" bomb adds special characters which poses problems later
"  set bomb
  setglobal fileencoding=utf-8
"  setglobal bomb
"  set fileencodings=ucs-bom,utf-8,latin1
endif

" number of commands saved in the history
set history=10000

" ignore case in searches
set ignorecase
" without case when search pattern contains only lowercase. works only when
" ignorecase is set
set smartcase

" while typing a search command, show immediately where the so far typed
" pattern matches
set incsearch

" wraps long lines visually
set wrap
" wraps around words, doesn't break a word in the middle
set linebreak

" when there is a previous search pattern, highlight all its matches
set hlsearch

" Save automatically the edition buffer before the commands :next or make
set autowrite

" Toggle line numbers and fold column for easy copying:
nnoremap <F2> :set nonumber!<CR>:set foldcolumn=0<CR>

" Open found file in a vertical split.
cnoremap vfind vertical sfind

" text indenting
set autoindent

" change default color scheme
colorscheme luna

" highlight cursor line (must come after colorscheme command to take effect)
set cursorline
set cursorcolumn

" See relative numbers
set relativenumber nu
" Let always one line above/below the cursor.
if !&scrolloff
    set scrolloff=1
endif

" how many columns vim uses when you hit Tab in insert mode
set softtabstop=2

" indentation width
set shiftwidth=2

" numbers of spaces of tab character.
set tabstop=2

" Characters leading wrapped lines.
set showbreak=+++

" replace tab with spaces
set expandtab

" show title in console title bar
set title

noremap <leader>qq :bdelete<cr>
noremap <leader>qQ :Bdelete<cr>

" Edit and use vimrc
noremap <leader>ev :vsplit ~/.vimrc<cr>
noremap <leader>sv :source ~/.vimrc<cr>

" Hide buffers when they are abandoned.
set hidden

" Copy directly from clipboard
noremap <leader>pc "+p
noremap <leader>Pc "+P

let xml_tag_completion_map = "»"

set mouse=a		" Enable mouse usage (all modes) in terminals

" Set a line in column 80, in order to check max line length (79)
au BufRead,BufNewFile *.py,*.rb setlocal colorcolumn=80
